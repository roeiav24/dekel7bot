import math

def do_turn(pw):
    if len(pw.my_fleets()) >= 1:
        return

    if len(pw.my_planets()) == 0 or len(pw.my_planets()) == len(pw.planets()):
        return
    
    for source in pw.my_planets():
        counter = 0
        dest = find_best_target(pw, 0, source)
        if (dest.owner == 2):
            req = dest.num_ships() + dest.growth_rate() * get_dist(source, dest) + 1
        else:
            req = dest.num_ships() + 1
        flag = True
        while source.num_ships() <= req:
            if counter == 3:
                flag = False
                break
            counter += 1
            dest = find_best_target(pw, counter, source)
            req = dest.num_ships() + dest.growth_rate() * get_dist(source, dest) + 1
        if flag:
            pw.issue_order(source, dest, req)def find_best_target(pw, time, source):
    all_planets = pw.enemy_planets() + pw.neutral_planets()
    dists = list()
    for i in range(len(all_planets)):
        dists.append(get_dist(source, all_planets[i]))

    for i in range(time + 1):
        closest = min(dists)
        index = dists.index(closest)
        if i == time:
            return all_planets[index]
        dists[index] = max(dists)
    


def get_dist(p1, p2):
    return math.sqrt((p1.x() - p2.x()) ** 2 + (p1.y() - p2.y()) ** 2)